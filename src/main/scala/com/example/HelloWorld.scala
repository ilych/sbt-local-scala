package com.example

import scala.collection.JavaConverters._

import com.google.gson.{Gson, GsonBuilder}

object HelloWorld extends App {
  val msg = Map(
    "message" -> "Hello, world!"
  )

  val builder = new GsonBuilder;
  val gson = builder.create();

  println(gson.toJson(msg.asJava))
}
