organization := "com.example"
name := "HelloWorld"
version := "0.1.0"
autoScalaLibrary := false
scalaHome := Some(file("."))
scalaVersion := "2.11.7-local"
unmanagedBase := file("lib")
javacOptions ++= Seq("-encoding", "UTF-8")

mainClass in (Compile, run) := Some("com.example.HelloWorld")
